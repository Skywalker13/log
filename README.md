
# Sources of [log.schroetersa.ch](http://log.schroetersa.ch)

Start the local hugo server.

```sh
./run.sh
```

Populate the `public/` output directory.

```sh
./build.sh
```

## Migration

Not all posts are (still) migrated from [skywalker13.wordpress.com](http://skywalker13.wordpress.com).
